#!/usr/bin/env bash

# Bind a local port 3306 and forward it to the mysql port 3306 on the cluster
kubectl -n orders-app port-forward --address 0.0.0.0 service/orders-mysql-svc 3306:3306
