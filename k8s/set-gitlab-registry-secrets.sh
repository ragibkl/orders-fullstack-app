#!/usr/bin/env bash

# save gitlab registry login credentials to k8s
read -p "username: "  username
read -s -p "password: "  password
echo ''

kubectl -n orders-app delete secret gitlab-registry-personal
kubectl -n orders-app create secret docker-registry gitlab-registry-personal \
    --docker-server=https://registry.gitlab.com \
    --docker-username=$username \
    --docker-password=$password
