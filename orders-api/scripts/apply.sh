#!/usr/bin/env bash

# Deploy orders-app to the cluster
kubectl -n orders-app apply -f orders-api.yaml
kubectl -n orders-app apply -f orders-deliveries-worker.yaml
kubectl -n orders-app apply -f orders-payments-worker.yaml
