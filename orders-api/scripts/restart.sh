#!/usr/bin/env bash

# Redeploy orders-app to the cluster
kubectl -n orders-app rollout restart deployment/orders-api
kubectl -n orders-app rollout restart deployment/orders-deliveries-worker
kubectl -n orders-app rollout restart deployment/orders-payments-worker
