#!/usr/bin/env bash

docker build -t registry.gitlab.com/ragibkl/orders-fullstack-app/orders-api .
docker push registry.gitlab.com/ragibkl/orders-fullstack-app/orders-api
