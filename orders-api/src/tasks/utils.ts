export const encodeData = (data: any): Buffer =>
  Buffer.from(JSON.stringify(data));

export const decodeData = (content: Buffer): Record<string, any> =>
  JSON.parse(content.toString());

export const sleep = (seconds: number): Promise<void> =>
  new Promise((resolve) => {
    setTimeout(resolve, seconds * 1000);
  });
