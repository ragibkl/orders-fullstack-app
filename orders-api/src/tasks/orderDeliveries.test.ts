import Order from '../db/orders';
import { handleOrderDeliveries } from './orderDeliveries';

describe('orderDeliveries', () => {
  describe('handleOrderDeliveries', () => {
    describe('when order is missing', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce(null);
        const log = jest.spyOn(console, 'log');

        await handleOrderDeliveries(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(log).toHaveBeenCalledWith(
          'handleOrderDeliveries: failed process',
          inputData,
          null,
        );
      });
    });

    describe('when order is still created state', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const mockOrder = {
          id: 1,
          status: 'created',
          save: jest.fn(),
        };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce((mockOrder as unknown) as Order);
        const log = jest.spyOn(console, 'log');

        await handleOrderDeliveries(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(log).toHaveBeenCalledWith(
          'handleOrderDeliveries: failed process',
          inputData,
          mockOrder,
        );
      });
    });

    describe('when order is cancelled state', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const mockOrder = {
          id: 1,
          status: 'created',
          save: jest.fn(),
        };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce((mockOrder as unknown) as Order);
        const log = jest.spyOn(console, 'log');

        await handleOrderDeliveries(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(log).toHaveBeenCalledWith(
          'handleOrderDeliveries: failed process',
          inputData,
          mockOrder,
        );
      });
    });

    describe('when order is confirmed state', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const mockOrder = {
          id: 1,
          status: 'confirmed',
          save: jest.fn(),
        };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce((mockOrder as unknown) as Order);
        const log = jest.spyOn(console, 'log');

        await handleOrderDeliveries(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(mockOrder.status).toBe('delivered');
        expect(mockOrder.save).toHaveBeenCalledTimes(1);
        expect(log).toHaveBeenCalledWith('handleOrderDeliveries', mockOrder);
      });
    });
  });
});
