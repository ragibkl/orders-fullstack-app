import { initModels } from '../db';
import Order, { OrderAttributes } from '../db/orders';
import { processOrderPayment } from '../services/payments';

import { subscribeTask, publishTask } from './rabbitmq';
import { publish as publishDelivery } from './orderDeliveries';

const QUEUE_NAME = 'order-payments';

const canPayOrder = (order: Order) => ['created'].includes(order.status);

export const publish = async (data: OrderAttributes) => {
  await publishTask(QUEUE_NAME, data);
};

export const handleOrderPayments = async (data: Record<string, any>) => {
  const orderData = data as OrderAttributes;
  const order = await Order.findByPk(orderData.id);

  if (!order || !canPayOrder(order)) {
    console.log('handleOrderPayments: failed process', orderData, order);
    return; // we can probably throw here?
  }

  const success = await processOrderPayment(orderData);
  if (success) {
    order.status = 'confirmed';
    order.save();
    await publishDelivery(order);
  } else {
    order.status = 'cancelled';
    order.save();
  }

  console.log('handleOrderPayments', order, success);
};

export const startWorker = async () => {
  await initModels();
  subscribeTask(QUEUE_NAME, handleOrderPayments);
};
