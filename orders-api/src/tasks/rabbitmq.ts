import amqp, { Connection } from 'amqplib';
import { config } from '../config';
import { decodeData, encodeData, sleep } from './utils';

type Data = Record<string, any>;
type Callback = (data: Data) => void | Promise<void>;

let instance: Connection | undefined;

export const createConnection = (): Promise<Connection> => {
  const { hostname, port, username, password } = config.rabbitmq;
  return amqp.connect({
    protocol: 'amqp',
    hostname,
    port,
    username,
    password,
    locale: 'en_US',
    frameMax: 0,
    heartbeat: 0,
    vhost: '/',
  });
};

export const getConnection = async (): Promise<Connection> => {
  if (!instance) {
    instance = await createConnection();
  }

  return instance;
};

export const subscribeTask = async (taskQueue: string, callback: Callback) => {
  const connection = await getConnection();
  const channel = await connection.createChannel();
  await channel.assertQueue(taskQueue);

  channel.consume(
    taskQueue,
    async (msg) => {
      if (msg) {
        await sleep(5); // simulate a slow busy worker
        try {
          const data = decodeData(msg.content);
          await callback(data);
          channel.ack(msg);
        } catch (error) {
          channel.nack(msg);
        }
      }
    },
    { noAck: false },
  );
};

export const publishTask = async (queue: string, data: Data) => {
  const connection = await getConnection();
  const channel = await connection.createChannel();
  await channel.assertQueue(queue);

  channel.sendToQueue(queue, encodeData(data));
};
