import { decodeData, encodeData } from './utils';

describe('utils', () => {
  describe('encodeData and decodeData', () => {
    describe('when called with data', () => {
      it('returns same object', () => {
        const inputData = { a: 123, b: 'asd', c: true, d: null };
        const buffer = encodeData(inputData);
        const outputData = decodeData(buffer);

        expect(buffer).toBeInstanceOf(Buffer);
        expect(outputData).toEqual(inputData);
      });
    });
  });
});
