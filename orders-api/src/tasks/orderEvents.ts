import { getConnection } from './rabbitmq';
import { encodeData } from './utils';

const EXCHANGE_NAME = 'order-events';

export const publish = async (id: number) => {
  const connection = await getConnection();
  const channel = await connection.createChannel();
  await channel.assertExchange(EXCHANGE_NAME, 'fanout', { durable: false });

  channel.publish(EXCHANGE_NAME, '', encodeData({ id }));
};
