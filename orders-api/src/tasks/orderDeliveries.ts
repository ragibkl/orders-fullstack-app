import { initModels } from '../db';
import Order, { OrderAttributes } from '../db/orders';

import { subscribeTask, publishTask } from './rabbitmq';

const QUEUE_NAME = 'order-deliveries';

const canDeliverOrder = (order: Order) => ['confirmed'].includes(order.status);

export const publish = async (data: OrderAttributes) => {
  await publishTask(QUEUE_NAME, data);
};

export const handleOrderDeliveries = async (data: Record<string, any>) => {
  const orderData = data as OrderAttributes;
  const order = await Order.findByPk(orderData.id);

  if (!order || !canDeliverOrder(order)) {
    console.log('handleOrderDeliveries: failed process', orderData, order);
    return; // we can probably throw here?
  }

  order.status = 'delivered';
  order.save();
  console.log('handleOrderDeliveries', order);
};

export const startWorker = async () => {
  await initModels();
  subscribeTask(QUEUE_NAME, handleOrderDeliveries);
};
