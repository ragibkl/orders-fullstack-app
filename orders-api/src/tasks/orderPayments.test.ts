import Order from '../db/orders';
import * as payments from '../services/payments';
import { handleOrderPayments } from './orderPayments';
import * as orderDeliveries from './orderDeliveries';

describe('orderPayments', () => {
  describe('handleOrderPayments', () => {
    afterEach(() => {
      jest.clearAllMocks();
      jest.resetAllMocks();
    });

    describe('when order is missing', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce(null);
        const processOrderPayment = jest
          .spyOn(payments, 'processOrderPayment')
          .mockResolvedValueOnce(true);
        const log = jest.spyOn(console, 'log');

        await handleOrderPayments(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(processOrderPayment).toHaveBeenCalledTimes(0);
        expect(log).toHaveBeenCalledWith(
          'handleOrderPayments: failed process',
          inputData,
          null,
        );
      });
    });

    describe('when order is cancelled state', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const mockOrder = {
          id: 1,
          status: 'cancelled',
          save: jest.fn(),
        };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce((mockOrder as unknown) as Order);
        const processOrderPayment = jest
          .spyOn(payments, 'processOrderPayment')
          .mockResolvedValue(true);
        const log = jest.spyOn(console, 'log');

        await handleOrderPayments(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(processOrderPayment).toHaveBeenCalledTimes(0);
        expect(log).toHaveBeenCalledWith(
          'handleOrderPayments: failed process',
          inputData,
          mockOrder,
        );
      });
    });

    describe('when order is created state, failed payment', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const mockOrder = {
          id: 1,
          status: 'created',
          save: jest.fn(),
        };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce((mockOrder as unknown) as Order);
        const processOrderPayment = jest
          .spyOn(payments, 'processOrderPayment')
          .mockResolvedValue(false);
        const log = jest.spyOn(console, 'log');

        await handleOrderPayments(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(processOrderPayment).toHaveBeenCalledTimes(1);
        expect(mockOrder.status).toBe('cancelled');
        expect(mockOrder.save).toHaveBeenCalledTimes(1);
        expect(log).toHaveBeenCalledWith(
          'handleOrderPayments',
          mockOrder,
          false,
        );
      });
    });

    describe('when order is created state, ok payment', () => {
      it('returns without processing', async () => {
        const inputData = { id: 1 };
        const mockOrder = {
          id: 1,
          status: 'created',
          save: jest.fn(),
        };
        const findByPk = jest
          .spyOn(Order, 'findByPk')
          .mockResolvedValueOnce((mockOrder as unknown) as Order);
        const processOrderPayment = jest
          .spyOn(payments, 'processOrderPayment')
          .mockResolvedValue(true);
        const publish = jest
          .spyOn(orderDeliveries, 'publish')
          .mockResolvedValue();
        const log = jest.spyOn(console, 'log');

        await handleOrderPayments(inputData);

        expect(findByPk).toHaveBeenCalledWith(1);
        expect(processOrderPayment).toHaveBeenCalledTimes(1);
        expect(publish).toHaveBeenCalledTimes(1);
        expect(publish).toHaveBeenCalledWith(mockOrder);
        expect(mockOrder.status).toBe('confirmed');
        expect(mockOrder.save).toHaveBeenCalledTimes(1);
        expect(log).toHaveBeenCalledWith(
          'handleOrderPayments',
          mockOrder,
          true,
        );
      });
    });
  });
});
