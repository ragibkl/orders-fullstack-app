import fetch from 'cross-fetch';
import { config } from '../config';

const { baseUrl } = config.payments;

type Order = {
  id: number;
  price: number;
};

export const processOrderPayment = async (order: Order): Promise<boolean> => {
  const url = `${baseUrl}/payments`;
  const data = {
    order_id: order.id,
    price: order.price,
    user_id: 1234,
    auth_code: 'blah-blah-blah',
  };

  const response = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data), // body data type must match "Content-Type" header
  });

  console.log(data, response);
  if (!response.ok) {
    throw new Error('Payments API failed!');
  }

  const { success } = await response.json();
  return success as boolean;
};
