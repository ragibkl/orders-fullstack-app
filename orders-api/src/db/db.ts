import { Sequelize } from 'sequelize';

import { config } from '../config';

export const createDb = (): Sequelize => {
  const { host, username, password, database, port } = config.mysql;

  return new Sequelize({
    dialect: 'mysql',
    database,
    host,
    password,
    port,
    username,
  });
};
