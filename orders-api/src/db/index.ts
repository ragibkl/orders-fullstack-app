import { Sequelize } from 'sequelize';

import { createDb } from './db';
import Order from './orders';

let sequelize: Sequelize | undefined;

export const initModels = async (): Promise<Sequelize> => {
  if (sequelize) {
    return sequelize;
  }

  sequelize = createDb();

  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
    throw error;
  }

  Order.setup(sequelize);

  return sequelize;
};
