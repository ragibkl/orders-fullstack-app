import { Sequelize, Model, DataTypes, Optional } from 'sequelize';
import { publish } from '../tasks/orderEvents';

export interface OrderAttributes {
  id: number;
  status: 'created' | 'confirmed' | 'delivered' | 'cancelled';
  description: string;
  price: number;
}

export type OrderCreationAttributes = Optional<OrderAttributes, 'id'>;

export default class Order
  extends Model<OrderAttributes, OrderCreationAttributes>
  implements OrderAttributes {
  // attributes
  public id!: number;

  public status!: 'created' | 'confirmed' | 'delivered' | 'cancelled';

  public description!: 'string';

  public price!: number;

  // timestamps
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  static setup(sequelize: Sequelize): void {
    const attributes = {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      status: {
        type: DataTypes.ENUM,
        values: ['created', 'confirmed', 'delivered', 'cancelled'],
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      price: {
        type: DataTypes.NUMBER,
        allowNull: false,
      },
    };

    const options = {
      sequelize,
      modelName: 'Order',
      tableName: 'orders',
      hooks: {
        afterSave: (order: Order) => {
          publish(order.id);
        },
      },
    };

    Order.init(attributes, options);
  }
}
