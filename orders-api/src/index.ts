import { initModels } from './db';
import { getConnection } from './tasks/rabbitmq';
import { app } from './app';

const PORT = 8000;

const main = async () => {
  console.log('main: initiating db models');
  await initModels();

  console.log('main: initiating rabbitmq connection');
  await getConnection();

  console.log('main: starting app');
  app.listen(PORT);
  console.log(`main: app listening on port: ${PORT}`);
};

main().catch((e) => {
  console.log('App exited unexpectedly!');
  console.log(e);
  process.exit(1);
});
