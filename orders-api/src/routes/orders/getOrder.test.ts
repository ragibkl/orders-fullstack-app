import supertest from 'supertest';

import Order from '../../db/orders';

import { app } from '../../app';

const request = supertest.agent(app.callback());

describe('getOrder', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe('when db returns null', () => {
    it('returns 40$', async () => {
      jest.spyOn(Order, 'findByPk').mockResolvedValue(null);
      const response = await request.get('/orders/1');

      expect(response.text).toEqual('Not Found');
      expect(response.status).toEqual(404);
    });
  });

  describe('when db returns an item', () => {
    it('returns 200', async () => {
      const mockOrder = ({
        get: () => ({
          id: 1,
          description: 'hello',
          status: 'created',
        }),
      } as unknown) as Order;

      jest.spyOn(Order, 'findByPk').mockResolvedValue(mockOrder);

      const response = await request.get('/orders/1');

      expect(response.body).toEqual(mockOrder.get());
      expect(response.status).toEqual(200);
    });
  });

  describe('when db throws error', () => {
    it('returns 500', async () => {
      jest.spyOn(Order, 'findByPk').mockRejectedValue(new Error('oops!'));

      const response = await request.get('/orders/1');

      expect(response.text).toEqual('Error: oops!');
      expect(response.status).toEqual(500);
    });
  });
});
