import supertest from 'supertest';

import Order from '../../db/orders';

import { app } from '../../app';

const request = supertest.agent(app.callback());

describe('getOrder', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe('when db returns null', () => {
    it('returns 404', async () => {
      jest.spyOn(Order, 'findByPk').mockResolvedValue(null);
      const response = await request.patch('/orders/1/cancel');

      expect(response.text).toEqual('Not Found');
      expect(response.status).toEqual(404);
    });
  });

  describe('when db returns created order', () => {
    it('returns 200', async () => {
      const mockData = {
        id: 1,
        description: 'hello',
        status: 'created',
      };

      const mockOrder = ({
        get: () => mockData,
        get status() {
          return mockData.status;
        },

        update: jest.fn().mockImplementation(async ({ status }) => {
          mockData.status = status;
        }),
      } as unknown) as Order;

      jest.spyOn(Order, 'findByPk').mockResolvedValue(mockOrder);

      const response = await request.patch('/orders/1/cancel');

      expect(mockOrder.update).toHaveBeenCalledWith({ status: 'canceled' });
      expect(mockOrder.update).toHaveBeenCalledTimes(1);
      expect(mockData.status).toBe('canceled');
      expect(response.body).toEqual(mockOrder.get());
      expect(response.status).toEqual(200);
    });
  });

  describe('when db returns delivered order', () => {
    it('returns 400', async () => {
      const mockData = {
        id: 1,
        description: 'hello',
        status: 'delivered',
      };

      const mockOrder = ({
        get: () => mockData,
        get status() {
          return mockData.status;
        },

        update: jest.fn().mockImplementation(async ({ status }) => {
          mockData.status = status;
        }),
      } as unknown) as Order;

      jest.spyOn(Order, 'findByPk').mockResolvedValue(mockOrder);

      const response = await request.patch('/orders/1/cancel');

      expect(mockOrder.update).toHaveBeenCalledTimes(0);
      expect(response.text).toEqual('Bad Request');
      expect(response.status).toEqual(400);
    });
  });

  describe('when db throws error', () => {
    it('returns 500', async () => {
      jest.spyOn(Order, 'findByPk').mockRejectedValue(new Error('oops!'));

      const response = await request.patch('/orders/1/cancel');

      expect(response.text).toEqual('Error: oops!');
      expect(response.status).toEqual(500);
    });
  });
});
