import { RouterContext } from 'koa-router';

import { publish } from '../../tasks/orderPayments';
import Order from '../../db/orders';
import { filterCreateOrderInput } from './types';

export const createOrder = async (ctx: RouterContext): Promise<void> => {
  let input;
  try {
    input = filterCreateOrderInput(ctx.request.body);
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
    return;
  }

  try {
    const order = await Order.create({
      status: 'created',
      description: input.description,
      price: input.price,
    });

    console.log(order);
    ctx.status = 200;
    ctx.body = order.get();

    publish(order.get());
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.body = error.toString();
  }
};
