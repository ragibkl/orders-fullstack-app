import { RouterContext } from 'koa-router';
import Order from '../../db/orders';

export const getOrders = async (ctx: RouterContext): Promise<void> => {
  try {
    const orders = await Order.findAll({
      order: [['id', 'desc']],
      limit: 20,
    });

    console.log(`orders.length: ${orders.length}`);
    ctx.status = 200;
    ctx.body = orders.map((o) => o.get());
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.body = error.toString();
  }
};
