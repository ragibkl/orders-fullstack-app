import * as t from 'runtypes';
import RuntypesFilter from 'runtypes-filter';

export const CreateOrderInput = t.Record({
  description: t.String,
  price: t.Number,
});

export const filterCreateOrderInput = RuntypesFilter(CreateOrderInput);

export type CreateOrderInput = t.Static<typeof CreateOrderInput>;
