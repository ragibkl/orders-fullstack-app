import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';

import { cancelOrder } from './cancelOrder';
import { createOrder } from './createOrder';
import { getOrder } from './getOrder';
import { getOrders } from './getOrders';

export const router = new Router();

router.use(bodyParser());
router.post('/', createOrder);
router.get('/', getOrders);

router.get('/:id', getOrder);
router.patch('/:id/cancel', cancelOrder);
