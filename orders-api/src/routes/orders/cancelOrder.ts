import { RouterContext } from 'koa-router';
import Order from '../../db/orders';

const canCancelOrder = (order: Order) =>
  ['created', 'confirmed'].includes(order.status);

export const cancelOrder = async (ctx: RouterContext): Promise<void> => {
  const { id } = ctx.params;

  try {
    const order = await Order.findByPk(id);

    if (!order) {
      ctx.status = 404;
      return;
    }

    if (!canCancelOrder(order)) {
      ctx.status = 400;
      return;
    }

    await order.update({ status: 'canceled' });

    ctx.status = 200;
    ctx.body = order.get();
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.body = error.toString();
  }
};
