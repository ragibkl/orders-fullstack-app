import supertest from 'supertest';

import * as orderPayments from '../../tasks/orderPayments';
import Order from '../../db/orders';

import { app } from '../../app';

const request = supertest.agent(app.callback());

describe('createOrder', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe('when empty body', () => {
    it('returns 400', async () => {
      jest.spyOn(Order, 'findByPk').mockResolvedValue(null);
      const response = await request.post('/orders/');

      expect(response.body).toEqual({
        key: 'description',
        name: 'ValidationError',
      });
      expect(response.status).toEqual(400);
    });
  });

  describe('when price is empty', () => {
    it('returns 400', async () => {
      const input = { description: 'hello' };
      jest.spyOn(Order, 'findByPk').mockResolvedValue(null);
      const response = await request.post('/orders/').send(input);

      expect(response.body).toEqual({
        key: 'price',
        name: 'ValidationError',
      });
      expect(response.status).toEqual(400);
    });
  });

  describe('when db returns created order', () => {
    it('returns 200', async () => {
      const input = {
        description: 'hello',
        price: 10,
      };

      const mockData = {
        id: 1,
        description: 'hello',
        status: 'created',
      };

      const mockOrder = ({
        get: () => mockData,
        get status() {
          return mockData.status;
        },
      } as unknown) as Order;

      jest
        .spyOn(Order, 'create')
        .mockResolvedValue((mockOrder as unknown) as void);

      jest.spyOn(orderPayments, 'publish').mockResolvedValue();

      const response = await request.post('/orders/').send(input);

      expect(response.body).toEqual(mockData);
      expect(response.status).toEqual(200);
    });
  });

  describe('when db throws error', () => {
    it('returns 500', async () => {
      const input = {
        description: 'hello',
        price: 10,
      };
      jest.spyOn(Order, 'create').mockRejectedValue(new Error('oops!'));
      jest.spyOn(orderPayments, 'publish').mockResolvedValue();

      const response = await request.post('/orders/').send(input);

      expect(response.text).toEqual('Error: oops!');
      expect(response.status).toEqual(500);
    });
  });
});
