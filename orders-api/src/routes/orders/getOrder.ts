import { RouterContext } from 'koa-router';
import Order from '../../db/orders';

export const getOrder = async (ctx: RouterContext): Promise<void> => {
  const { id } = ctx.params;

  try {
    const order = await Order.findByPk(id);

    if (!order) {
      ctx.status = 404;
      return;
    }

    console.log(order);

    ctx.status = 200;
    ctx.body = order.get();
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.body = error.toString();
  }
};
