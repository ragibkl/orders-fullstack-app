import supertest from 'supertest';

import Order from '../../db/orders';

import { app } from '../../app';

const request = supertest.agent(app.callback());

describe('getOrders', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe('when db returns empty', () => {
    it('returns 200', async () => {
      jest.spyOn(Order, 'findAll').mockResolvedValue([]);
      const response = await request.get('/orders');

      expect(response.body).toEqual([]);
      expect(response.status).toEqual(200);
    });
  });

  describe('when db returns 1 item', () => {
    it('returns 200', async () => {
      const mockOrder = ({
        get: () => ({
          id: 1,
          description: 'hello',
          status: 'created',
        }),
      } as unknown) as Order;

      jest.spyOn(Order, 'findAll').mockResolvedValue([mockOrder]);

      const response = await request.get('/orders');

      expect(response.body).toEqual([mockOrder.get()]);
      expect(response.status).toEqual(200);
    });
  });

  describe('when db throws error', () => {
    it('returns 500', async () => {
      jest.spyOn(Order, 'findAll').mockRejectedValue(new Error('oops!'));

      const response = await request.get('/orders');

      expect(response.text).toEqual('Error: oops!');
      expect(response.status).toEqual(500);
    });
  });
});
