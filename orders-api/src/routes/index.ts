import Router from 'koa-router';

import { router as healthRouter } from './health';
import { router as ordersRouter } from './orders';

export const router = new Router();

router.use('/health', healthRouter.routes());
router.use('/orders', ordersRouter.routes(), ordersRouter.allowedMethods());
