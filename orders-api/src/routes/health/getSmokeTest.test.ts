import supertest from 'supertest';
import { Sequelize } from 'sequelize';
import { Connection } from 'amqplib';

import * as db from '../../db/db';
import * as rabbitmq from '../../tasks/rabbitmq';

import { app } from '../../app';

const request = supertest.agent(app.callback());

describe('getSmokeTest', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe('calling get', () => {
    it('returns 200', async () => {
      const mockSequelize = ({
        authenticate: jest.fn(),
        close: jest.fn(),
      } as unknown) as Sequelize;

      const mockConnection = ({
        authenticate: jest.fn(),
        close: jest.fn(),
      } as unknown) as Connection;

      jest.spyOn(db, 'createDb').mockReturnValue(mockSequelize);
      jest
        .spyOn(rabbitmq, 'createConnection')
        .mockResolvedValue(mockConnection);
      const response = await request.get('/health/smoke');

      expect(response.text).toEqual('Smoke Test OK from orders-api');
      expect(response.status).toEqual(200);
    });
  });
});
