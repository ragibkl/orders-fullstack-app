import { RouterContext } from 'koa-router';

import { createDb } from '../../db/db';
import { createConnection } from '../../tasks/rabbitmq';

export const getSmokeTest = async (ctx: RouterContext): Promise<void> => {
  const sequelize = createDb();
  try {
    await sequelize.authenticate();
    await sequelize.close();
  } catch (error) {
    ctx.status = 500;
    ctx.body = `Unable to connect to the database: ${error}`;
    return;
  }

  try {
    const connection = await createConnection();
    await connection.close();
  } catch (error) {
    ctx.status = 500;
    ctx.body = `Unable to connect to the message queue: ${error}`;
    return;
  }

  ctx.body = 'Smoke Test OK from orders-api';
  ctx.status = 200;
};
