import { RouterContext } from 'koa-router';

export const getHealthCheck = (ctx: RouterContext): void => {
  ctx.body = 'Health Check OK from orders-api';
  ctx.status = 200;
};
