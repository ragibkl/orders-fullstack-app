import Koa from 'koa';
import cors from '@koa/cors';

import { router } from './routes';

export const app = new Koa();

const ALLOWED_ORIGINS: Record<string, true> = {
  'http://localhost:3000': true,
  'https://orders.apps.bancuh.net': true,
};

const origin = (ctx: Koa.Context): string => {
  const header = ctx.get('origin');
  if (ALLOWED_ORIGINS[header]) {
    return header;
  }

  return '';
};

app.use(cors({ origin }));
app.use(router.routes());
app.use(router.allowedMethods());
