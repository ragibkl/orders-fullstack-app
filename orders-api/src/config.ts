export const config = {
  mysql: {
    host: process.env.MYSQL_HOST || '127.0.0.1',
    port: process.env.MYSQL_PORT ? parseInt(process.env.MYSQL_PORT, 10) : 3306,
    username: process.env.MYSQL_USER || 'user',
    password: process.env.MYSQL_PASSWORD || 'password',
    database: process.env.MYSQL_DATABASE || 'orders_db',
  },
  rabbitmq: {
    hostname: process.env.AMQP_HOSTNAME || 'localhost',
    port: process.env.AMQP_PORT ? parseInt(process.env.AMQP_PORT, 10) : 5672,
    username: process.env.AMQP_USERNAME || 'guest',
    password: process.env.AMQP_PASSWORD || 'guest',
  },
  payments: {
    baseUrl: process.env.PAYMENTS_BASEURL || 'http://localhost:8001',
  },
};
