#!/usr/bin/env bash

# Deploy orders-events to the cluster
kubectl -n orders-app apply -f orders-events.yaml
