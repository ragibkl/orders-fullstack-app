#!/usr/bin/env bash

# Redeploy orders-events to the cluster
kubectl -n orders-app rollout restart deployment/orders-events
