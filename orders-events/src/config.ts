export const config = {
  rabbitmq: {
    hostname: process.env.AMQP_HOSTNAME || 'localhost',
    port: process.env.AMQP_PORT ? parseInt(process.env.AMQP_PORT, 10) : 5672,
    username: process.env.AMQP_USERNAME || 'guest',
    password: process.env.AMQP_PASSWORD || 'guest',
  },
};
