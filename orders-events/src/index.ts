import { Server, Socket } from 'socket.io';
import { startWorker } from './events/orderEvents';

const io = new Server({
  serveClient: false,
  cors: {
    methods: ['GET', 'POST'],
    origin: ['http://localhost:3000', 'https://orders.apps.bancuh.net'],
  },
});

io.on('connection', (socket: Socket) => {
  console.log(`${socket.id} - connected`);
  socket.on('disconnect', () => {
    console.log(`${socket.id} - disconnected`);
  });
});

const broadcastOrdersUpdated = (id: number) => {
  console.log('broadcast', id);
  io.emit('orders-updated', id);
};

io.listen(8002);
startWorker(broadcastOrdersUpdated);
