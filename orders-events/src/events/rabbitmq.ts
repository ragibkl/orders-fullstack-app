import amqp, { Connection } from 'amqplib';
import { config } from '../config';

let instance: Connection | undefined;

export const createConnection = (): Promise<Connection> => {
  const { hostname, port, username, password } = config.rabbitmq;
  return amqp.connect({
    protocol: 'amqp',
    hostname,
    port,
    username,
    password,
    locale: 'en_US',
    frameMax: 0,
    heartbeat: 0,
    vhost: '/',
  });
};

export const getConnection = async (): Promise<Connection> => {
  if (!instance) {
    instance = await createConnection();
  }

  return instance;
};
