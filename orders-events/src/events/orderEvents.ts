import { getConnection } from './rabbitmq';

const EXCHANGE_NAME = 'order-events';

type Callback = (id: number) => void | Promise<void>;

export const startWorker = async (cb: Callback) => {
  const connection = await getConnection();
  const channel = await connection.createChannel();
  await channel.assertExchange(EXCHANGE_NAME, 'fanout', { durable: false });
  const q = await channel.assertQueue('', { exclusive: true });
  channel.bindQueue(q.queue, EXCHANGE_NAME, '');

  channel.consume(
    q.queue,
    async (msg) => {
      if (msg) {
        try {
          const { id } = JSON.parse(msg.content.toString());
          cb(id);
          channel.ack(msg);
        } catch (error) {
          channel.nack(msg);
        }
      }
    },
    { noAck: false },
  );
};
