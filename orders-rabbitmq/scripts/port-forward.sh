#!/usr/bin/env bash

# Bind a local port 5672 and forward it to the rabbitmq port 5672 on the cluster
kubectl -n orders-app port-forward --address 0.0.0.0 service/orders-rabbitmq-svc 5672:5672
