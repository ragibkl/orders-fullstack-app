import { io } from 'socket.io-client';
import { config } from '../config';

type Callback = () => void | Promise<void>;

const socket = io(config.ordersEventsUrl);

export const subscribeOrdersUpdated = (callback: Callback) => {
  socket.on('orders-updated', callback);

  const unsubscribe = () => {
    socket.off('orders-updated', callback);
  };
  return unsubscribe;
};

export const subscribeOrderUpdatedById = (
  orderId: number,
  callback: Callback,
) => {
  const wrappedCallback = (id: number) => {
    if (id === orderId) {
      callback();
    }
  };
  socket.on('orders-updated', wrappedCallback);

  const unsubscribe = () => {
    socket.off('orders-updated', wrappedCallback);
  };
  return unsubscribe;
};
