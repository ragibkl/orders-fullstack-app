import { config } from '../config';

export interface OrderAttributes {
  id: number;
  status: string;
  description: string;
  price: number;
}

export interface CreateOrderAttributes {
  description: string;
  price: number;
}

export async function createOrder(input: CreateOrderAttributes) {
  const response = await fetch(`${config.ordersBaseUrl}/orders/`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(input),
  });
  const data = await response.json();
  return data as OrderAttributes;
}

export async function fetchOrders() {
  const response = await fetch(`${config.ordersBaseUrl}/orders/`);
  const data = await response.json();
  return data as Array<OrderAttributes>;
}

export async function fetchOrderById(id: number) {
  const response = await fetch(`${config.ordersBaseUrl}/orders/${id}`);
  const data = await response.json();
  return data as OrderAttributes;
}

export async function cancelOrderById(id: number) {
  const response = await fetch(`${config.ordersBaseUrl}/orders/${id}/cancel`, {
    method: 'PATCH',
  });
  const data = await response.json();
  return data as OrderAttributes;
}
