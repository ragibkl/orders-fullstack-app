import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Box, Breadcrumbs, Button, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Add as AddIcon } from '@material-ui/icons';

import { fetchOrders, OrderAttributes } from '../../services/orders';
import { subscribeOrdersUpdated } from '../../services/ordersEvents';
import OrdersTable from './OrdersTable';
import Header from './Header';

const useStyles = makeStyles({
  wrapper: {
    padding: 20,
  },
});

export default function OrdersPage() {
  const [orders, setOrders] = useState<Array<OrderAttributes>>([]);

  useEffect(() => {
    const loadOrders = async () => {
      const data = await fetchOrders();
      setOrders(data);
    };
    loadOrders();

    return subscribeOrdersUpdated(loadOrders);
  }, []);

  const classes = useStyles();

  return (
    <Container>
      <Header />
      <Breadcrumbs></Breadcrumbs>
      <Box className={classes.wrapper}>
        <Button
          variant="contained"
          color="primary"
          startIcon={<AddIcon />}
          component={Link}
          to="/orders/new"
        >
          Create Order
        </Button>
      </Box>
      <Box>
        <OrdersTable orders={orders} />
      </Box>
    </Container>
  );
}
