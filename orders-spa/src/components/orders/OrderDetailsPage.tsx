import React, { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Container,
  Paper,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useParams } from 'react-router-dom';

import Header from './Header';
import {
  OrderAttributes,
  fetchOrderById,
  cancelOrderById,
} from '../../services/orders';
import { Delete as DeleteIcon } from '@material-ui/icons';
import { subscribeOrderUpdatedById } from '../../services/ordersEvents';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    padding: 20,
  },
});

const canCancelOrder = (order: OrderAttributes): boolean => {
  return ['created', 'confirmed'].includes(order.status);
};

export default function OrdersCreatePage() {
  const classes = useStyles();
  const { id } = useParams<{ id: string }>();
  const idInt = parseInt(id);

  const [order, setOrder] = useState<OrderAttributes | undefined>();
  useEffect(() => {
    const loadOrderDetail = async () => {
      const data = await fetchOrderById(idInt);
      setOrder(data);
    };
    loadOrderDetail();

    return subscribeOrderUpdatedById(idInt, loadOrderDetail);
  }, [idInt]);

  const cancelOrder = async () => {
    await cancelOrderById(idInt);
    const data = await fetchOrderById(idInt);
    setOrder(data);
  };

  return (
    <Container>
      <Header />
      <Paper className={classes.root}>
        {order ? (
          <>
            <Typography variant="h5">Order Details</Typography>

            <Typography variant="body1" color="textPrimary">
              ID
            </Typography>
            <Typography variant="body2" color="textSecondary" gutterBottom>
              {order.id}
            </Typography>

            <Typography variant="body1" color="textPrimary">
              Description
            </Typography>
            <Typography variant="body2" color="textSecondary" gutterBottom>
              {order.description}
            </Typography>

            <Typography variant="body1" color="textPrimary">
              Price
            </Typography>
            <Typography variant="body2" color="textSecondary" gutterBottom>
              {order.price}
            </Typography>

            <Typography variant="body1" color="textPrimary">
              Status
            </Typography>
            <Typography variant="body2" color="textSecondary" gutterBottom>
              {order.status}
            </Typography>

            <Box paddingTop={'20px'}>
              <Button
                variant="contained"
                color="secondary"
                startIcon={<DeleteIcon />}
                onClick={cancelOrder}
                disabled={!canCancelOrder(order)}
              >
                Cancel Order
              </Button>
            </Box>
          </>
        ) : (
          <CircularProgress />
        )}
      </Paper>
    </Container>
  );
}
