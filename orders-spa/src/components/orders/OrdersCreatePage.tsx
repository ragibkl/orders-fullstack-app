import React from 'react';
import { Container } from '@material-ui/core';

import Header from './Header';
import OrderCreateForm from './OrderCreateForm';

export default function OrdersCreatePage() {
  return (
    <Container>
      <Header />
      <OrderCreateForm />
    </Container>
  );
}
