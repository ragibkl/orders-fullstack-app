import React, { useState, ChangeEvent } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import { createOrder } from '../../services/orders';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function OrderCreateForm() {
  const classes = useStyles();

  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);

  const onChangeDescription = (e: ChangeEvent<HTMLInputElement>) =>
    setDescription(e.target.value);
  const onChangePrice = (e: ChangeEvent<HTMLInputElement>) =>
    setPrice(parseInt(e.target.value));

  const history = useHistory();
  const onSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    const order = await createOrder({
      description,
      price,
    });
    history.push(`/orders/${order.id}`);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Create Order
        </Typography>
        <form className={classes.form} noValidate onSubmit={onSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="description"
            label="Description"
            name="description"
            autoFocus
            onChange={onChangeDescription}
            value={description}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="price"
            label="Price"
            type="number"
            id="price"
            onChange={onChangePrice}
            value={price}
          />

          <Box display="block">
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Create
            </Button>
          </Box>
        </form>
      </div>
    </Container>
  );
}
