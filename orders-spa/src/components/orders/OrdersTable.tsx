import React from 'react';
import { Link } from 'react-router-dom';
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from '@material-ui/core';

import { OrderAttributes } from '../../services/orders';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

type Props = {
  orders: Array<OrderAttributes>;
};

export default function OrdersTable({ orders }: Props) {
  const classes = useStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="left">ID</TableCell>
            <TableCell>Description</TableCell>
            <TableCell align="right">Status</TableCell>
            <TableCell align="right">Price</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orders.map((order) => (
            <TableRow key={order.id}>
              <TableCell align="left" scope="row">
                <Link to={`/orders/${order.id}`}>{order.id}</Link>
              </TableCell>
              <TableCell>{order.description}</TableCell>
              <TableCell align="right">{order.status}</TableCell>
              <TableCell align="right">{order.price}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
