import React from 'react';
import { CssBaseline } from '@material-ui/core';
import AppRouter from './AppRouter';

export default function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <AppRouter />
    </React.Fragment>
  );
}
