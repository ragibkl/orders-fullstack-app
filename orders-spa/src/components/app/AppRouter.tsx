import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import OrderDetailsPage from '../orders/OrderDetailsPage';
import OrdersCreatePage from '../orders/OrdersCreatePage';
import OrdersPage from '../orders/OrdersPage';

export default function AppRouter() {
  return (
    <Router>
      <Switch>
        <Route path="/orders/new">
          <OrdersCreatePage />
        </Route>
        <Route path="/orders/:id">
          <OrderDetailsPage />
        </Route>
        <Route path="/orders">
          <OrdersPage />
        </Route>
        <Route path="/">
          <Redirect to="/orders" />
        </Route>
      </Switch>
    </Router>
  );
}
