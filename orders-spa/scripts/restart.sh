#!/usr/bin/env bash

# Redeploy orders-spa to the cluster
kubectl -n orders-app rollout restart deployment/orders-spa
