# orders-fullstack-app

Full stack orders app

# Overview

This is a demo project, based on the **2018.02.27-fullstack-engineer-practical** provided to me to assess my technical approach to problems. A copy of the question set can be found in [here](docs/2018.02.27-fullstack-engineer-practical.pdf).

We are tasked to create an Orders Management Portal of sorts. The system allows for a user view a list of orders, and click to view a detail page of an order. the user may also create a new order, or cancel an existing order.

# Live Version

There is a live version of this project running at [here](https://orders.apps.bancuh.net).

The public endpoints for this project are as follows:

- Frontend SPA: https://orders.apps.bancuh.net
- Orders API: https://orders-api.apps.bancuh.net
- Orders Events WebSocket: https://orders-events.apps.bancuh.net

# Project Architecture

![alt text](docs/architecture.drawio.png)

This project consists of 8 components. Each component is built as a Docker image, that can be run as separate docker containers.

I have opted to deploy this project on kubernetes (k8s). K8s provides me the flexibility of running containers and configuring ingress routes and services as I see fit. It also allows me to configure the deployment easily with a few scripts, directly from my local machine.

I have also opted to run the database and message queue using publicly available `mysql` and `rabbitmq` Docker images whithin the k8s cluster. While they are not as robust as major cloud offerings, they are practically free, and easy to setup, allowing me to rapidly prototype this project quickly and cheaply.

## orders-mysql

SQL database using mysql Docker image.

## orders-rabbitmq

Message broker and queue service, using official Docker image. I use this for 3 purposes:

1. `order-payments` message queue

When and order is newly created in `orders-api`, it inserts a message into this queue. This is later picked up by `orders-payments-worker` which processes this asynchronously.

2. `order-deliveries` message queue

When and order has confirmed payment in `orders-payments-worker`, it inserts a message into this queue. This is later picked up by `orders-deliveries-worker` which processes this asynchronously.

3. `order-events` broadcast channel

Whenever an order is created/updated, a message is inserted here. It is then broadcasted to `order-events`, which would relay the message to frontend clients.

## orders-api

This component consists of an API and 2 worker runtimes. This allows me to share the sequelize model among the runtimes, but also unfortunately tightly couples them together.

The ORM model has an hook configured. When any order object is created/updated, the ORM model publishes a message to the `order-events` channel.

I use `koa` for building the http listener for the API.

I use `sequelize` for modelling the stored data in `mysql`

I use `amqplib` for connecting to `rabbitmq`

### API

Public Endpoint: https://orders-api.apps.bancuh.net

Api for managing orders.

### orders-payments-worker

Picks up newly created order, and processes them asynchronously. Calls `payments-api` to submit a payment, and updates the order info based on the payment result.

### orders-deliveries-worker

Picks up newly confirmed order, and processes them asynchronously. Just moves them to `delivered` state after a few seconds.

## orders-events

Public Endpoint: https://orders-events.apps.bancuh.net

Websocket endpoint to which the `orders-spa` connect to from the frontend.

This component picks up any `order-events` messages from `orders-rabbitmq`, and relays them to the frontend. This is used to tell the frontend to fetch for updated orders data.

I use `amqplib` for connecting to `rabbitmq`

I use `socket.io` for the websocket implementation.

## orders-spa

Public Endpoint: https://orders.apps.bancuh.net

Frontend client in React JS, using the common create-react-app toolkit.

I use `material-ui` to prototype the UI.

I use `socket.io-client` for connecting to the `orders-events` websocket endpoint.

## payments-api

Simple api to simulate payments processing by and external service. Randomly returns `success` or `fail` result.

I use `koa` for building the http listener for the API.
