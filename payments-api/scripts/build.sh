#!/usr/bin/env bash

docker build -t registry.gitlab.com/ragibkl/orders-fullstack-app/payments-api .
docker push registry.gitlab.com/ragibkl/orders-fullstack-app/payments-api
