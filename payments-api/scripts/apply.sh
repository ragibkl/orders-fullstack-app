#!/usr/bin/env bash

# Deploy payments-api to the cluster
kubectl -n orders-app apply -f payments-api.yaml
