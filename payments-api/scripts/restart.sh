#!/usr/bin/env bash

# Redeploy payments-api to the cluster
kubectl -n orders-app rollout restart deployment/payments-api
