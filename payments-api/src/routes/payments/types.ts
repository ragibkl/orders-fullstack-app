import * as t from 'runtypes';
import RuntypesFilter from 'runtypes-filter';

export const CreatePaymentInput = t.Record({
  order_id: t.Number,
  price: t.Number,
});

export const filterCreatePaymentInput = RuntypesFilter(CreatePaymentInput);

export type CreatePaymentInput = t.Static<typeof CreatePaymentInput>;
