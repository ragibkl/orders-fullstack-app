import { RouterContext } from 'koa-router';

import { processPayment } from '../../services/payments';
import { filterCreatePaymentInput } from './types';

export const createPayment = async (ctx: RouterContext): Promise<void> => {
  try {
    filterCreatePaymentInput(ctx.request.body);
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
    return;
  }

  try {
    const paymentResult = await processPayment();

    ctx.status = 200;
    ctx.body = {
      success: paymentResult,
    };
    console.log('payment created', ctx.body);
  } catch (error) {
    console.log(error);
    ctx.status = 500;
    ctx.body = error.toString();
  }
};
