import supertest from 'supertest';

import * as payments from '../../services/payments';

import { app } from '../../app';

const request = supertest.agent(app.callback());

describe('createPayments', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe('when input is empty', () => {
    it('returns 400', async () => {
      const response = await request.post('/payments/');

      expect(response.body).toEqual({
        key: 'order_id',
        name: 'ValidationError',
      });
      expect(response.status).toEqual(400);
    });
  });

  describe('when payments returns true', () => {
    it('returns 200', async () => {
      const input = {
        order_id: 3,
        user_id: 123,
        auth_code: 'blah-blah-blah',
        price: 100,
      };
      jest.spyOn(payments, 'processPayment').mockResolvedValue(true);
      const response = await request.post('/payments/').send(input);

      expect(response.body).toEqual({ success: true });
      expect(response.status).toEqual(200);
    });
  });

  describe('when payments returns false', () => {
    it('returns 200', async () => {
      const input = {
        order_id: 3,
        user_id: 123,
        auth_code: 'blah-blah-blah',
        price: 100,
      };
      jest.spyOn(payments, 'processPayment').mockResolvedValue(false);
      const response = await request.post('/payments/').send(input);

      expect(response.body).toEqual({ success: false });
      expect(response.status).toEqual(200);
    });
  });

  describe('when payments throws error', () => {
    it('returns 500', async () => {
      const input = {
        order_id: 3,
        user_id: 123,
        auth_code: 'blah-blah-blah',
        price: 100,
      };
      jest
        .spyOn(payments, 'processPayment')
        .mockRejectedValue(new Error('oops!'));
      const response = await request.post('/payments/').send(input);

      expect(response.text).toEqual('Error: oops!');
      expect(response.status).toEqual(500);
    });
  });
});
