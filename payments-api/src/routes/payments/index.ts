import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';

import { createPayment } from './createPayment';

export const router = new Router();

router.use(bodyParser());
router.post('/', createPayment);
