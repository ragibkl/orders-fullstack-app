import Router from 'koa-router';

import { getHealthCheck } from './getHealthCheck';
import { getSmokeTest } from './getSmokeTest';

export const router = new Router();

router.get('/health', getHealthCheck);
router.get('/smoke', getSmokeTest);
