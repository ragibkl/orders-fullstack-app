import { RouterContext } from 'koa-router';

export const getSmokeTest = async (ctx: RouterContext): Promise<void> => {
  ctx.body = 'Smoke Test OK from payments-api';
  ctx.status = 200;
};
