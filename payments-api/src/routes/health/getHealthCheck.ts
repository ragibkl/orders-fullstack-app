import { RouterContext } from 'koa-router';

export const getHealthCheck = (ctx: RouterContext): void => {
  ctx.body = 'Health Check OK from payments-api';
  ctx.status = 200;
};
