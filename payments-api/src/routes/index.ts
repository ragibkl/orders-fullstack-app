import Router from 'koa-router';

import { router as healthRouter } from './health';
import { router as paymentsRouter } from './payments';

export const router = new Router();

router.use('/health', healthRouter.routes());
router.use(
  '/payments',
  paymentsRouter.routes(),
  paymentsRouter.allowedMethods(),
);
