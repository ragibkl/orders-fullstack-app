import random from 'random';

type PaymentResult = boolean;

const chooseRandom = <T extends unknown>(choices: Array<T>): T => {
  const getIndex = random.uniformInt(0, choices.length - 1);
  return choices[getIndex()];
};

export const processPayment = async (): Promise<PaymentResult> => {
  const choices: Array<boolean> = [true, false];
  return chooseRandom(choices);
};
